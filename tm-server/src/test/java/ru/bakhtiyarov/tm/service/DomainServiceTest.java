package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.service.IDomainService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.constant.DataConst;
import ru.bakhtiyarov.tm.marker.UnitCategory;

import java.io.File;
import java.nio.file.Files;

@Category(UnitCategory.class)
public class DomainServiceTest {

 /*   @NotNull
    private final static ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final static IDomainService domainService = new DomainService(serviceLocator);

    @BeforeClass
    public static void addData() {
        serviceLocator.getUserService().create("test", "test");
        serviceLocator.getProjectService().create("userID", "project");
        serviceLocator.getTaskService().create("userID", "task");
    }

    @Test
    @SneakyThrows
    public void testLoadAndSaveBase64() {
        Assert.assertEquals(1, serviceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getUserService().findAll().size());
        domainService.saveBase64();
        ServiceLocator testServiceLocator = new Bootstrap();
        Assert.assertEquals(0, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getUserService().findAll().size());
        testServiceLocator.getDomainService().loadBase64();
        Assert.assertEquals(1, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getUserService().findAll().size());
        serviceLocator.getDomainService().clearBase64();
    }

    @Test
    @SneakyThrows
    public void testLoadAndSaveBinary() {
        Assert.assertEquals(1, serviceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getUserService().findAll().size());
        domainService.saveBinary();
        ServiceLocator testServiceLocator = new Bootstrap();
        Assert.assertEquals(0, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getUserService().findAll().size());
        testServiceLocator.getDomainService().loadBinary();
        Assert.assertEquals(1, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getUserService().findAll().size());
        serviceLocator.getDomainService().clearBinary();
    }

    @Test
    @SneakyThrows
    public void testLoadAndSaveJson() {
        Assert.assertEquals(1, serviceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getUserService().findAll().size());
        domainService.saveJson();
        ServiceLocator testServiceLocator = new Bootstrap();
        Assert.assertEquals(0, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getUserService().findAll().size());
        testServiceLocator.getDomainService().loadJson();
        Assert.assertEquals(1, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getUserService().findAll().size());
        serviceLocator.getDomainService().clearJson();
    }

    @Test
    @SneakyThrows
    public void testLoadAndSaveXml() {
        Assert.assertEquals(1, serviceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getUserService().findAll().size());
        domainService.saveXml();
        ServiceLocator testServiceLocator = new Bootstrap();
        Assert.assertEquals(0, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getUserService().findAll().size());
        testServiceLocator.getDomainService().loadXml();
        Assert.assertEquals(1, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getUserService().findAll().size());
        serviceLocator.getDomainService().clearXml();
    }

    @Test
    @SneakyThrows
    public void testLoadAndSaveYaml() {
        Assert.assertEquals(1, serviceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, serviceLocator.getUserService().findAll().size());
        domainService.saveYaml();
        ServiceLocator testServiceLocator = new Bootstrap();
        Assert.assertEquals(0, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(0, testServiceLocator.getUserService().findAll().size());
        testServiceLocator.getDomainService().loadYaml();
        Assert.assertEquals(1, testServiceLocator.getTaskService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getProjectService().findAll().size());
        Assert.assertEquals(1, testServiceLocator.getUserService().findAll().size());
        serviceLocator.getDomainService().clearYaml();
    }

    @Test
    @SneakyThrows
    public void testClearBase64() {
        serviceLocator.getDomainService().saveBase64();
        final File file = new File(DataConst.FILE_BASE64);
        Assert.assertTrue(Files.exists(file.toPath()));
        serviceLocator.getDomainService().clearBase64();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    @SneakyThrows
    public void testClearBinary() {
        serviceLocator.getDomainService().saveBinary();
        final File file = new File(DataConst.FILE_BINARY);
        Assert.assertTrue(Files.exists(file.toPath()));
        serviceLocator.getDomainService().clearBinary();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    @SneakyThrows
    public void testClearJson() {
        serviceLocator.getDomainService().saveJson();
        final File file = new File(DataConst.FILE_JSON);
        Assert.assertTrue(Files.exists(file.toPath()));
        serviceLocator.getDomainService().clearJson();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    @SneakyThrows
    public void testClearXml() {
        serviceLocator.getDomainService().saveXml();
        final File file = new File(DataConst.FILE_XML);
        Assert.assertTrue(Files.exists(file.toPath()));
        serviceLocator.getDomainService().clearXml();
        Assert.assertFalse(Files.exists(file.toPath()));
    }

    @Test
    @SneakyThrows
    public void testClearYaml() {
        serviceLocator.getDomainService().saveYaml();
        final File file = new File(DataConst.FILE_YAML);
        Assert.assertTrue(Files.exists(file.toPath()));
        serviceLocator.getDomainService().clearYaml();
        Assert.assertFalse(Files.exists(file.toPath()));
    }
*/
}
