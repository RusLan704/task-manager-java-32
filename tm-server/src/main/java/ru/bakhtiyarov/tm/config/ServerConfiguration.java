package ru.bakhtiyarov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.bakhtiyarov.tm.api.service.IManagerFactoryService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Configuration
@ComponentScan("ru.bakhtiyarov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IManagerFactoryService entityManagerFactoryService
    ) {
        return entityManagerFactoryService.buildFactory();
    }

    @Bean
    @NotNull
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public EntityManager entityManager(
            @NotNull final EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

}
