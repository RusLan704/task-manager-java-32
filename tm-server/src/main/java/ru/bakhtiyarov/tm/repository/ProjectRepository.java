package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> removeAll(@NotNull final String userId) {
       @NotNull final List<Project> projects = findAllByUserId(userId);
       projects.forEach(entityManager::remove);
       return projects;
    }

    @NotNull
    @Override
    public List<Project> removeAll() {
        @NotNull final List<Project> projects = findAllByUserId();
        projects.forEach(this::remove);
        return projects;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        List<Project> projects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(index);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery(
                "SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        List<Project> projects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        List<Project> projects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if (projects.isEmpty()) return null;
        return projects.get(0);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

}
