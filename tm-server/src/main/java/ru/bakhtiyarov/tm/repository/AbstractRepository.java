package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.bakhtiyarov.tm.api.repository.IRepository;
import ru.bakhtiyarov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void begin() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void commit() {
        entityManager.getTransaction().commit();
    }

    @Override
    public void rollback() {
        entityManager.getTransaction().rollback();
    }

    @Override
    public void close() {
        entityManager.close();
    }

    @Override
    public void addAll(@NotNull List<E> records) {
        records.forEach(this::merge);
    }

    @NotNull
    @Override
    public E merge(@NotNull final E e) {
        entityManager.merge(e);
        return e;
    }

    @NotNull
    @Override
    public E persist(@NotNull E record) {
        entityManager.persist(record);
        return record;
    }

    @Override
    public E remove(@NotNull E record) {
        entityManager.remove(record);
        return record;
    }

}