package ru.bakhtiyarov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bakhtiyarov.tm.api.endpoint.IProjectEndpoint;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.dto.SessionDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.service.converter.ProjectConverter;
import ru.bakhtiyarov.tm.service.converter.SessionConverter;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
@Controller
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private SessionConverter sessionConverter;

    @Autowired
    private ProjectConverter projectConverter;

    @Override
    @SneakyThrows
    public void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @SneakyThrows
    public void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Override
    @SneakyThrows
    public void removeAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        projectService.removeAll(session.getUserId());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneById(session.getUserId(), id);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAllProjectsBySession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable List<Project> projects = projectService.findAll(sessionDTO.getUserId());
        return projects
                .stream()
                .map(projectConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneByIndex(session.getUserId(), index);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.findOneByName(session.getUserId(), name);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @SneakyThrows
    @Nullable
    public ProjectDTO removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.removeOneByIndex(session.getUserId(), index);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.removeOneById(session.getUserId(), id);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.removeOneByName(session.getUserId(), name);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {

        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.updateProjectById(session.getUserId(), id, name, description);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        @Nullable Session session = sessionConverter.toEntity(sessionDTO);
        sessionService.validate(session);
        @Nullable Project project = projectService.updateProjectByIndex(session.getUserId(), index, name, description);
        @Nullable ProjectDTO projectDTO = projectConverter.toDTO(project);
        return projectDTO;
    }

}
