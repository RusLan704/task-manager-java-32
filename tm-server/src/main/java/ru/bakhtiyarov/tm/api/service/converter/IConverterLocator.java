package ru.bakhtiyarov.tm.api.service.converter;

import org.jetbrains.annotations.NotNull;

public interface IConverterLocator {

    @NotNull
    ITaskConverter getTaskConverter();

    @NotNull
    IProjectConverter getProjectConverter();

    @NotNull
    IUserConverter getUserConverter();

    @NotNull
    ISessionConverter getSessionConverter();

}
