package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IManagerFactoryService {

    @NotNull
    EntityManagerFactory buildFactory();

}
