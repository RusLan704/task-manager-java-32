package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserId();

    @NotNull
    List<Project> removeAll(@NotNull String userId);

    @NotNull
    List<Project> removeAll();

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeOneByName(@NotNull String userId, @NotNull String name);

}
