package ru.bakhtiyarov.tm.api.service.converter;

import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.dto.ProjectDTO;
import ru.bakhtiyarov.tm.entity.Project;

public interface IProjectConverter {

    @Nullable
    ProjectDTO toDTO(@Nullable Project project);

    @Nullable
    Project toEntity(@Nullable ProjectDTO projectDTO);

}
