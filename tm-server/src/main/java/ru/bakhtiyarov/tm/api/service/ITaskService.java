package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String projectName, @Nullable String taskName);

    void create(@Nullable String userId, @Nullable String projectName, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectId);

    @Nullable
    List<Task> removeAll();

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void removeAll(@Nullable String userId);

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    List<Task> removeAllByUserIdAndProjectName(@Nullable final String userId, @Nullable final String projectId);

}
