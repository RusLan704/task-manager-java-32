package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.util.HashUtil;
import ru.bakhtiyarov.tm.util.SignatureUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionService extends AbstractService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable String login, @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();

        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();

        @NotNull ISessionRepository repository = getRepository();
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        session.setUser(user);
        @Nullable Session resultSession = null;
        try {
            repository.begin();
            resultSession = repository.persist(session);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return resultSession;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session sign(@Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null) throw new AccessDeniedException();
        session.setSignature(signature);
        return session;
    }

    @SneakyThrows
    public void validate(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String userId = user.getId();
        @NotNull ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void signOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeAll(userId);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void close(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @NotNull ISessionRepository repository = getRepository();
        try {
            repository.begin();
            repository.remove(session);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean closeAll(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        validate(session);
        @NotNull ISessionRepository repository = getRepository();
        boolean response = false;
        try {
            repository.begin();
            response = repository.removeAll(session.getUserId());
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return response;
    }

    @NotNull
    @Override
    public List<Session> removeAll() {
        @NotNull ISessionRepository repository = getRepository();
        List<Session> sessions = new ArrayList<>();
        try {
            repository.begin();
            sessions = repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull ISessionRepository repository = getRepository();
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable Session session) {
        validate(session);
        @NotNull ISessionRepository taskRepository = getRepository();
        return taskRepository.findAll(session.getUserId());
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
