package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.empty.*;
import ru.bakhtiyarov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository repository = getRepository();
        User user = null;
        try {
            repository.begin();
            user = repository.removeByLogin(login);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository repository = getRepository();
        User user = null;
        try {
            repository.begin();
            user = repository.removeById(id);
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> removeAll() {
        @NotNull IUserRepository repository = getRepository();
        List<User> users = new ArrayList<>();
        try {
            repository.begin();
            users = repository.removeAll();
            repository.commit();
        } catch (Exception e) {
            repository.rollback();
        } finally {
            repository.close();
        }
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return persist(user);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull IUserRepository repository = getRepository();
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setRole(Role.USER);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return persist(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updatePassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserEmail(@Nullable final String id, @Nullable final String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setEmail(email);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserFirstName(@Nullable final String id, @Nullable final String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserLastName(@Nullable final String id, @Nullable final String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setLastName(lastName);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserMiddleName(@Nullable final String id, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setMiddleName(middleName);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUserLogin(@Nullable final String id, @Nullable final String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(id);
        if (user == null) return null;
        user.setLogin(login);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull IUserRepository repository = getRepository();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull IUserRepository repository = getRepository();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return merge(user);
    }

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

}