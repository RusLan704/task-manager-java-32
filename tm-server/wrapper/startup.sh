#!/bin/bash

PORT="8080"
if [ -n "$1" ]; then
  PORT=$1
fi

FILE_PID="tm-server-$PORT"
if [ -f ./"$FILE_PID".pid ]; then
  echo "Server already started!"
  exit 1;
fi

mkdir -p ../log
rm -f ../log/"$FILE_PID".log
nohup java -Dport="$PORT" -jar ./tm-server.jar > ../log/"$FILE_PID".log 2>&1 &
echo $! > "$FILE_PID".pid
echo "STARTING TASK MANAGER SERVER WITH PID "$!
