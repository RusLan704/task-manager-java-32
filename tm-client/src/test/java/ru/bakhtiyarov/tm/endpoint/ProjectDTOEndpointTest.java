package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.api.locator.EndpointLocator;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.marker.IntegrationCategory;

import javax.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectDTOEndpointTest {

//    @NotNull
//    private static final EndpointLocator endpointLocator = new Bootstrap();
//
//    @NotNull
//    private static ProjectEndpoint projectEndpoint;
//
//    @NotNull
//    private static SessionDTO session;
//
//    @NotNull
//    private ProjectDTO project = new ProjectDTO();
//
//    @BeforeClass
//    public static void initData() {
//        projectEndpoint = endpointLocator.getProjectEndpoint();
//        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
//    }
//
//    @AfterClass
//    public static void closeSession() {
//        endpointLocator.getSessionEndpoint().closeSessionAll(session);
//    }
//
//    @Before
//    public void addProject() {
//        project.setName("name");
//        project.setDescription("description");
//        project.setId("123");
//        project.setUserId(session.getUserId());
//        projectEndpoint.addProject(session, project);
//    }
//
//    @After
//    public void clearData() {
//        projectEndpoint.clearAllProjects(session);
//    }
//
//    @Test
//    public void testAddAllProjects() {
//        List<ProjectDTO> projects = new ArrayList<>();
//        projects.add(new ProjectDTO());
//        projects.add(new ProjectDTO());
//        projects.add(new ProjectDTO());
//        projectEndpoint.addAllProjects(session, projects);
//        List<ProjectDTO> testProjects = projectEndpoint.findAllProjects(session);
//        Assert.assertNotNull(testProjects);
//        Assert.assertEquals(4, testProjects.size());
//    }
//
//    @Test
//    public void testAdd() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.addProject(session, project);
//        Assert.assertEquals(2, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//
//    @Test
//    public void testCreateProjectByName() {
//        projectEndpoint.createProjectByName(session, "name");
//        Assert.assertEquals(2, projectEndpoint.findAllProjectsBySession(session).size());
//        ProjectDTO project = projectEndpoint.findProjectOneByName(session, "name");
//        Assert.assertNotNull(project);
//        Assert.assertEquals("name", project.getName());
//    }
//
//    @Test
//    public void createProjectByNameDescription() {
//        projectEndpoint.createProjectByNameDescription(session, "name", "description");
//        ProjectDTO project = projectEndpoint.findProjectOneByName(session, "name");
//        Assert.assertNotNull(project);
//        Assert.assertEquals("name", project.getName());
//        Assert.assertEquals("description", project.getDescription());
//    }
//
//    @Test
//    public void testRemoveOneById() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.removeProjectOneById(session, project.getId());
//        Assert.assertEquals(0, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//    @Test
//    public void testRemoveByIndex() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.removeProjectOneByIndex(session, 0);
//        Assert.assertEquals(0, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//    @Test
//    public void testRemoveByName() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.removeProjectOneByName(session, "name");
//        Assert.assertEquals(0, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//    @Test
//    public void testClearProjectBySession() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.clearAllProjectsBySession(session);
//        Assert.assertEquals(0, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//    @Test
//    public void testClearAllProjects() {
//        Assert.assertEquals(1, projectEndpoint.findAllProjectsBySession(session).size());
//        projectEndpoint.clearAllProjects(session);
//        Assert.assertEquals(0, projectEndpoint.findAllProjectsBySession(session).size());
//    }
//
//    @Test
//    public void testUpdateById() {
//        ProjectDTO testProject = projectEndpoint.updateProjectById(session, project.getId(), "test1", "desc");
//        Assert.assertNotNull(testProject);
//        Assert.assertEquals(project.getId(), testProject.getId());
//        Assert.assertEquals("test1", testProject.getName());
//        Assert.assertEquals("desc", testProject.getDescription());
//    }
//
//    @Test
//    public void testUpdateByIndex() {
//        ProjectDTO testProject = projectEndpoint.updateProjectByIndex(session, 0, "test1", "desc");
//        Assert.assertNotNull(testProject);
//        Assert.assertEquals(project.getId(), testProject.getId());
//        Assert.assertEquals("test1", testProject.getName());
//        Assert.assertEquals("desc", testProject.getDescription());
//    }
//
//    @Test
//    public void testFindAll() {
//        projectEndpoint.addProject(session, project);
//        projectEndpoint.addProject(session, project);
//        projectEndpoint.addProject(session, project);
//        projectEndpoint.addProject(session, project);
//        projectEndpoint.addProject(session, project);
//        projectEndpoint.addProject(session, project);
//        List<ProjectDTO> projects = projectEndpoint.findAllProjectsBySession(session);
//        Assert.assertNotNull(projects);
//        Assert.assertEquals(7, projects.size());
//    }
//
//    @Test
//    public void testFindByName() {
//        ProjectDTO testProject = projectEndpoint.findProjectOneByName(session, "name");
//        Assert.assertNotNull(testProject);
//        Assert.assertEquals(project.getId(), testProject.getId());
//    }
//
//    @Test
//    public void testFindOneById() {
//        ProjectDTO testProject = projectEndpoint.findProjectOneById(session, project.getId());
//        Assert.assertNotNull(testProject);
//        Assert.assertEquals(project.getId(), testProject.getId());
//    }
//
//    @Test
//    public void testFindByIndex() {
//        ProjectDTO testProject = projectEndpoint.findProjectOneByIndex(session, 0);
//        Assert.assertNotNull(testProject);
//        Assert.assertEquals(project.getId(), testProject.getId());
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameWithNullSession() {
//        projectEndpoint.createProjectByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameWithNullName() {
//        projectEndpoint.createProjectByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameWithEmptyName() {
//        projectEndpoint.createProjectByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameDescriptionWithNullSession() {
//        projectEndpoint.createProjectByNameDescription(null, "name", "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameDescriptionWithNullName() {
//        projectEndpoint.createProjectByNameDescription(session, null, "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameDescriptionWithEmptyName() {
//        projectEndpoint.createProjectByNameDescription(session, "", "description");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameDescriptionWithNullDescription() {
//        projectEndpoint.createProjectByNameDescription(session, "name", null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testCreateProjectByNameDescriptionWithEmptyDescription() {
//        projectEndpoint.createProjectByNameDescription(session, "name", "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeCreateProjectByNameWithNullSession() {
//        projectEndpoint.clearAllProjects(null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeAddProjectWithNullSession() {
//        projectEndpoint.addProject(null, project);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeAddAllProjectsWithNullSession() {
//        projectEndpoint.addAllProjects(null, new ArrayList<>());
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByNameWithNullSession() {
//        projectEndpoint.removeProjectOneByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByNameWithEmptySession() {
//        projectEndpoint.removeProjectOneByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByNameWithNullName() {
//        projectEndpoint.removeProjectOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByIndexWithNullSession() {
//        projectEndpoint.removeProjectOneByIndex(null, 0);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByIndexWithNullName() {
//        projectEndpoint.removeProjectOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByIdWithNullSession() {
//        projectEndpoint.removeProjectOneById(null, "123");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByIdWithNullId() {
//        projectEndpoint.removeProjectOneById(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeRemoveProjectOneByIdWithEmptyId() {
//        projectEndpoint.removeProjectOneById(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindAllProjectsWithNullSession() {
//        projectEndpoint.findAllProjectsBySession(null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByNameWithNullSession() {
//        projectEndpoint.findProjectOneByName(null, "name");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByNameWithEmptyName() {
//        projectEndpoint.findProjectOneByName(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByNameWithNullName() {
//        projectEndpoint.findProjectOneByName(session, null);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByIndexWithNullSession() {
//        projectEndpoint.findProjectOneByIndex(null, 0);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByIndexWithMinusIndex() {
//        projectEndpoint.findProjectOneByIndex(null, -2);
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByIdWithNullSession() {
//        projectEndpoint.findProjectOneById(null, "123");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByIdWithEmptyId() {
//        projectEndpoint.findProjectOneById(session, "");
//    }
//
//    @Test(expected = WebServiceException.class)
//    public void testNegativeFindProjectOneByIdWithNullName() {
//        projectEndpoint.findProjectOneById(session, null);
//    }

}



