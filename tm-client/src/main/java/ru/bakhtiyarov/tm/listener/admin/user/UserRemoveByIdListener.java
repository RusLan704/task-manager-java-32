package ru.bakhtiyarov.tm.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminUserEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.UserDTO;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserRemoveByIdListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user in program.";
    }

    @Override
    @EventListener(condition = "@userRemoveByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DELETE USER]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        UserDTO user = adminUserEndpoint.removeUserById(session, id);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
