package ru.bakhtiyarov.tm.listener.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataYamlLoadListener extends AbstractListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from YAML file.";
    }

    @Override
    @EventListener(condition = "@dataYamlLoadListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA YAML LOAD]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.loadYaml(session);
        System.out.println("[OK]");
    }

}

