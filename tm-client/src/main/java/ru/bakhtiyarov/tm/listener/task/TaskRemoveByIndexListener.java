package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_REMOVE_BY_INDEX;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @EventListener(condition = "@taskRemoveByIndexListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        @NotNull SessionDTO session = sessionService.getSession();
        @Nullable final TaskDTO task = taskEndpoint.removeTaskOneByIndex(session, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}