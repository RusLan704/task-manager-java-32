package ru.bakhtiyarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.ProjectEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class ProjectClearListener extends AbstractListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CLEAR PROJECT]");
        @NotNull SessionDTO session = sessionService.getSession();
        projectEndpoint.removeAllProjectsBySession(session);
        System.out.println("[OK]");
    }

}
