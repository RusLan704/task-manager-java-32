package ru.bakhtiyarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.bootstrap.Bootstrap;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.List;

@Component
public final class ShowListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commandList;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    @EventListener(condition = "@showListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        for (@NotNull AbstractListener command : commandList) {
            System.out.println(command.name());
        }
    }

}