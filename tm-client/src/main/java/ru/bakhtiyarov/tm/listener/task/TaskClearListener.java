package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;

@Component
public final class TaskClearListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_CLEAR;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @EventListener(condition = "@taskClearListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CLEAR TASKS]");
        @NotNull SessionDTO session = sessionService.getSession();
        taskEndpoint.clearAllTasksBySession(session);
        System.out.println("[OK]");
    }

}
