package ru.bakhtiyarov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.SessionEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class LoginListener extends AbstractListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;
    
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login user in program";
    }

    @EventListener(condition = "@loginListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final SessionDTO openedSession = sessionEndpoint.openSession(login, password);
        sessionService.setSession(openedSession);
        System.out.println("[OK]");
    }

}
