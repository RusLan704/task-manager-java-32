package ru.bakhtiyarov.tm.listener.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.AdminDataEndpoint;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;

@Component
public final class DataXmlSaveListener extends AbstractListener {

    @Autowired
    private AdminDataEndpoint adminDataEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data in XML file.";
    }

    @Override
    @EventListener(condition = "@dataXmlSaveListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[DATA XML SAVE]");
        @NotNull SessionDTO session = sessionService.getSession();
        adminDataEndpoint.saveXml(session);
        System.out.println("[OK]");
    }

}
